import time

from datetime import datetime, timedelta, timezone
from threading import Thread

import pymongo
import schedule

from submarine.ConnectionModule.src import CLIENT_CONNECTION_REFUSED_ERROR_MESSAGE
from submarine.ConnectionModule.src.ClientNode import resolve_destination, resend_message
from submarine.PersistenceModule.src import MESSAGE_RESEND_INTERVAL_IN_MINUTES, DATABASE_NAME, MESSAGE_COLLECTION, \
    MESSAGE_SAVED_TIME_IN_DAYS, DATABASE_HOSTNAME, DATABASE_PORT
from submarine.PersistenceModule.src.DatabaseConnection import get_client, get_db, get_collection


def handle_resend_message(message) -> str:
    destination = resolve_destination(message)
    response = resend_message(message, destination[0], destination[1])
    return response


def setup_resend_saved_messages():
    print("Executing job for resending messages")
    now = datetime.now(timezone.utc)
    too_old_date = now - timedelta(days=MESSAGE_SAVED_TIME_IN_DAYS)

    database_client = get_client(DATABASE_HOSTNAME, DATABASE_PORT)
    submarine_database = get_db(database_client, DATABASE_NAME)
    collection = get_collection(submarine_database, MESSAGE_COLLECTION)

    list_with_messages_from_the_database = \
        list(submarine_database.get_collection(MESSAGE_COLLECTION).find({}).sort("_id", pymongo.DESCENDING))

    resend_saved_messages(collection, list_with_messages_from_the_database, too_old_date)


def resend_saved_messages(collection, message_list, too_old_date) -> None:
    for single_message in message_list:
        time_message_was_sent = single_message['_id'].generation_time
        del single_message['_id']

        if time_message_was_sent < too_old_date:
            delete_message(collection, single_message)
        else:
            response = handle_resend_message(single_message['message'])

            if response != CLIENT_CONNECTION_REFUSED_ERROR_MESSAGE:
                delete_message(collection, single_message)


def execute_schedule(interval_in_minutes) -> None:
    schedule.every(interval_in_minutes).minutes.do(setup_resend_saved_messages)
    while True:
        schedule.run_pending()
        time.sleep(1)


def delete_message(collection, message):
    collection.delete_one(message)


def start_schedulers() -> None:
    scan_the_database_for_messages = Thread(target=execute_schedule,
                                            kwargs={'interval_in_minutes': MESSAGE_RESEND_INTERVAL_IN_MINUTES})
    scan_the_database_for_messages.start()
