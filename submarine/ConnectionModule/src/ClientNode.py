import json
import socket

from submarine.ConnectionModule.src import CONNECTION_REFUSED_ERROR_MESSAGE, CLIENT_CONNECTION_REFUSED_ERROR_MESSAGE, \
    PORT
from submarine.ConnectionModule.src.HttpRequests import do_http_request_with_body
from submarine.PersistenceModule.src.PersistMessage import get_ip_from_alias, persist_message


def create_connection_with_node(ip, port) -> socket:
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(5)
        s.connect((ip, port))
        return s
    except (ConnectionRefusedError, TimeoutError, socket.timeout) as e:
        raise ConnectionRefusedError(e, CONNECTION_REFUSED_ERROR_MESSAGE)


def resolve_destination(message) -> [str, int]:
    ip_dict = {
        "127.0.0.1": "127.0.0.1",
        "94.124.143.122": "10.20.3.122",
        "94.124.143.132": "10.20.3.132",
        "94.124.143.139": "10.20.3.139",
        "192.168.99.100": "192.168.99.100",
        "asd-p1-server1.asd.icaprojecten.nl": "10.20.3.122",
        "asd-p1-server2.asd.icaprojecten.nl": "10.20.3.132",
        "asd-p1-server3.asd.icaprojecten.nl": "10.20.3.139"
    }

    message = json.loads(message)
    dest = message['destination']

    if 'alias' in dest:
        print(dest)
        ip = get_ip_from_alias(message['destination']['alias'])
        port = PORT
    else:
        ip = ip_dict[dest['hostname']]
        port = dest['port']

    return ip, port


def resend_message(message, ip, port) -> str:
    try:
        s = create_connection_with_node(ip, port)
        message_to_be_sent = str(len(message)) + message
        s.sendall(message_to_be_sent.encode())
    except ConnectionRefusedError:
        return CLIENT_CONNECTION_REFUSED_ERROR_MESSAGE
    s.recv(8080)
    s.close()


def send_message(message, ip, port) -> str:
    try:
        s = create_connection_with_node(ip, port)
        message_to_be_sent = str(len(message)) + message
        s.sendall(message_to_be_sent.encode())
    except ConnectionRefusedError:
        persist_message(message)
        return CLIENT_CONNECTION_REFUSED_ERROR_MESSAGE
    s.recv(8080)
    s.close()


def send_incoming_request(message) -> str:
    json_message = json.loads(message)

    data = json.loads(json_message['data'])
    result = do_http_request_with_body(
        data['endpoint'], data['request_type'], json.dumps(data['body']))
    return str(result)


def send_incoming_request_relay(message, ip, port) -> str:
    with create_connection_with_node(ip, port) as s:
        try:
            message_to_be_sent = str(len(message)) + message
            s.sendall(message_to_be_sent.encode())
            data = s.recv(8080).decode()
            print(data)
            s.close()
            return data
        except ConnectionRefusedError:
            s.close()
            return CLIENT_CONNECTION_REFUSED_ERROR_MESSAGE


if __name__ == "__main__":
    IP = '127.0.0.1'
    PORT = 25010

    full_onion = '{"destination":{"hostname":"127.0.0.1","port":25010},"command":"RELAY","data":"AXSqAXRLvEHQSHjLP3QFk9ny5OWxEs8adbdhPXD80ZZvxekRPVEJwfPNmxqenZ5fUP/n/at4QYAal69w4PKDiGn+RzIzamxNtME1Y7080KSxnB+lugcorkP8cfvC3scSuOh4+sFcTZBj/3XrX/sHCfI4D42gwctH8y5/3kZ2EaAijjEfH1t71O7+ii8kSFko9idruYwWDUKAdESnJA3kJTVYtDrCzXkeVM6SnnjH1DcWi18mY18qtKj9qdVGSxpAuNBlwNl4Z31OAYGTLvhypmYLQcz33ED0PucK3hrwXz1Ew9iEaIQskGX6b/QeUu/N7hUbNBSKR/u/nS8Pl5ccaTQXkCVanpycLS9XhIuG+VDVkjnY9Mg7VoQt2kxEZGBH7KlyDMJuBN+zWCPUWkGOTbEaPmBI08faHNuIYY9wbJ4Lo+sKWBC9swM+hr+j2ThEcx+2OeFv1hDdSKS0HkMNsXhiy8JwYtq2k/RHk3//rw23gh1T/Rf0n3bQZeVLf/+5E2bb98e7dITqNU7HqT3RtJ6rnd5DaNg2g4TgdlMTpPPZSGTiODcVOD9W+xPBmZVccHNozJ7yj1DgImdagsPGNUxPQMxjqWygMj0POykWDGc="}'
    full_onion_http_request = '{"destination":{"hostname":"127.0.0.1","port":25010},"command":"HTTP_RELAY","data":"AXSqAXRLvEHQSHjLP3QFk9ny5OWxEs8adbdhPXD80ZZvxekRPVEJwfPNmxqenZ5fUP/n/at4QYAal69w4PKDiJ32KVLbctV/ImsL6nZg+8b5pfn36r5KcvtAMdiq5pYyXLsLgitjoRVP4hnt21nYBtR5IG4Fxza7TSfiEoIGAfnOwdFka7FctRtPe2v+qF2xv09e3niAN8hpfEr27SWwca5p5T/zAX5jxz1VN/raZZ1rxUYMBAODPojqUFTUuITGummvM8/8SI0kX89w7XmVh7DqXQXTTuORdwsZtI4uIK2R9tTFjITwAZa4NHFcH7lZcbzXywBoqkDnBxeyebUR8GG7ElhHcTrvRy61mQVqwm5QZN86Tn3kh79VRaomi8DwIM2yzTwr0VDNMLLUrLDgApTrqXUEXgM+uGVfDkFmzW7cKF4KnWlukTCNsVWZWvWgbCu5fZJqF44SLyUgoFnidzniUPBBW3hd3k1a4cIG6asCEaRyL3JlCOYwvkM96tLT2r1+ThgNHmJ0VxOhfLyKO1ycsm9EYYaTGoGPwsKWPKepxgsw29m+UD0jeluQA0mJb5/9fywGO41RlItWe2+A+sXNCv6hmt5vVFjBgn+/NK9UaRAqSQixNnwnEeM1/lAStIfCChWd9fSZxv+Y9NVDyrp/8OZofwaG0CLZu3FaXJGjarl8dz686hdELgtfT42KbB7RTbhEYVk+uJC2siNN0UJ3bdk/3uQPKN9goysgk4mBTSt3EPiTV8KJLJnct6Yo/PGFexqq+bkYHd0zrFTyl/nQET5xVUesoArB7485Bk4Jn7eVDyu9O5xkkjG0zu+t8lmO8bOiOm3BSLpp5A3iIdvIep8bMcLkQ8aR75gMEVyBGSCPgs0+R4qYeZa+HN1QrxGWOt5WwpwwDcaUSN7swcMgjk6JdJmd1d91z32zwD4Av1yqBk3GDungNrTvBytplrVEXpT6EDf0y8rIR+5nTWFLnLqFleHnCluy6uPlDcA+ac0g/Pdt/F+ecfd//1pbu5LEY5Lhp5ztnwRhUfew0w=="}'

    destination = resolve_destination(full_onion)
    send_message(full_onion, destination[0], destination[1])
